import state from "./stateQuickHelp";
import mutations from "./mutationsQuickHelp";
import getters from "./gettersQuickHelp";

/**
 * QuickHelp state.
 */
export default {
    namespaced: true,
    state,
    mutations,
    getters
};
